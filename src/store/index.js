import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    contactsArray: [],
    modalContent: null,
    localization: {
      'fullname': 'Имя',
      'company': 'Компания',
      'email': 'Email',
      'uname': 'Username',
      'address': 'Адрес',
      'streetAddress': 'Улица',
      'city': 'Город',
      'state': 'Штат',
      'zip': 'ZIP-код',
    }
  },
  getters: {
    getContacts: state => state.contactsArray,
    getContactById: state => id => state.contactsArray.find(el => el.id === id),
    getModalContent: state => state.modalContent,
    getLocalization: state => state.localization
  },
  mutations: {
    setContacts: (state, data) => {
      state.contactsArray = data;
    },
    setModalContent: (state, data) => {
      state.modalContent = data;
    }
  },
  actions: {
    getContacts: ({ commit }) => {
      return new Promise((resolve, reject) => {
        axios.get("http://www.filltext.com/?rows=1000&id={index}&fullname={firstName}~{lastName}&company={business}&email={email}&uname={username}&address={addressObject}")
          .then((response) => {
            commit('setContacts', response.data)
            resolve(response)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  },
  modules: {

  }
})
